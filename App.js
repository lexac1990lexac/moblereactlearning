import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import RegisterPage from './screens/register';
import { Button } from 'react-native';
import Template from './styles/template';
import LoginPage from './screens/Login';
import ForgotPassword from './screens/ForgotPassword'

const Stack = createNativeStackNavigator();

export default function App() {

  return (
    <NavigationContainer>
  
      <Stack.Navigator initialRouteName= 'ForgotPassword' >
        <Stack.Screen name="Register" component={RegisterPage} options={{
          headerTitle: "",
          headerBackTitle: "Назад",
          headerStyle: Template.invisibleHeader,
          contentStyle: Template.pageStyle,
          headerShown: false
        }}/>
        <Stack.Screen name="Login" component={LoginPage} options={{
          headerShown: false,
        }}/>
        <h1>Некит даун</h1>
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} options={{
          headerShown: false,
        }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
