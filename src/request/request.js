import constants from '../../constants/index';

export default class Request {

    static xmlRequest = new XMLHttpRequest();
    static _data = {};

    async fetchNews() {
        var url = constants.api.API_ROOT + constants.api.NEWS_URL;

        var response = await this.get(url);

        return response.data;
    }

    async get(url) {
        try {
            const response = await fetch(url);

            const json = await response.json();

            return {
                status: true,
                data: json
            };
        } catch(error) {
            return {
                status: false,
                message: error
            }
        }
    }
}