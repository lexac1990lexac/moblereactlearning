import { StyleSheet, NativeModules, Platform, Dimensions } from 'react-native';

const {width: WIDTH, height:  HEIGHT} = Dimensions.get('window');

export default StyleSheet.create({
    AndroidSafeArea: {
        backgroundColor: "#000",
        paddingTop: Platform.OS === "android" ? NativeModules.StatusBarManager.HEIGHT : 0,
    },

    invisibleHeader: {
        backgroundColor: "#000",
    },
    pageStyle: {
        backgroundColor: "#000",
        color: "white",
        paddingHorizontal: 45,
        paddingBottom: 0
    },

    centered: {
        alignSelf: "center"
    },

    defaultText: {
        color: "white",
        fontSize: 18,
    },

    defaultInput: {
        color: "white",
        paddingHorizontal: 15,
        paddingVertical: 10,
        fontSize: 16,
        backgroundColor: "#212121",
        borderRadius: 10,
    },

    inputLabel: {
        paddingBottom: 5
    },

    formGroup: {
        marginTop: 15
    },

    defaultButton: {
        borderRadius: 10,
        marginTop: 20,
        backgroundColor: "#ffbb3b",
        paddingVertical: 15,
        alignItems: "center"
    },

    defaultButtonText: {
        color: "black",
        fontSize: 18,
    },
    smallHeader: {
        color: "#BFBFBF",
        fontSize: 13,
        textAlign: 'center',
    },
    defaultHeader: {
        color: "white",
        fontSize: 18,
        fontWeight: 'bold',
    },

    delimiterLine: {
        backgroundColor: '#6a6e6b', 
        height: 2, 
        flex: 1,
        alignSelf: 'center'
    },

    delimiterText: {
        alignSelf:'center', 
        paddingHorizontal:15, 
        fontSize: 14,
        color: "#a8a8a8"
    },

    delimiterWrap: {
        flexDirection: 'row',
        marginTop: 33
    },

    smallText: {
        fontSize: 14
    },

    upperCase: {
        textTransform: "uppercase"
    },

    backgroundImageOpacity: {
        width: WIDTH,
        height: HEIGHT,
        backgroundColor: "#000",
        opacity: 0.65,
        position: "absolute"
    },

    alignCenter: {
        alignItems: "center",
        justifyContent: "center"
    },

    backButton: {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 15
    },

    backButtonText: {
        marginBottom: 2
    },
    wrap: {
        justifyContent: "center",
        height: HEIGHT
    },
});