import { StyleSheet, NativeModules, Platform } from 'react-native';

export default (props) => StyleSheet.create({
    circle: {
        width: props.width ?? 150,
        height: props.height ?? 150,
        borderRadius: 9999,
        backgroundColor: props.bgColor ?? "white",
        flex: props.centered ? 1 : 0,
        justifyContent: "center",
        alignContent: "center",
    }
});