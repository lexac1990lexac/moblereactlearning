import { StyleSheet, Dimensions } from 'react-native';

const {width: WIDTH, height: HEIGHT} = Dimensions.get('window')

export default StyleSheet.create({
    backgroundContainer: {
      width: WIDTH,
      height: HEIGHT,
      alignItems: "center",
      justifyContent: "center"
    },

    pageBody: {
      width: WIDTH,
      height: HEIGHT,
      alignItems: "center",
      justifyContent: "center"
    },

    logoContainer:{
      alignItems:'center',
    },

    logo: {
      alignSelf: "center",
    },

    form: {
      marginTop: 77,
      width: WIDTH,
      paddingHorizontal: 45
    },

    socialLinks: {
      flexDirection: "row",
      marginTop: 23,
      alignSelf: "center"
    },

    socialLinkImage: {
      width: 55,
      height: 55,
      marginHorizontal: 15
    },

    createAccountBlock: {
      marginTop: 46,
      alignItems: "center"
    },

    registerButton: {
      fontSize: 16,
      padding: 10,
      fontWeight: "bold"
    },

    registerButtonHeader: {
      opacity: 0.6392156862745098,
      fontSize: 16
    }
});