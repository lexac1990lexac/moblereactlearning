import { StyleSheet, Dimensions } from 'react-native';

const {width: WIDTH, height: HEIGHT} = Dimensions.get('window')

export default StyleSheet.create({
    BackgroundContainer: {
      height: HEIGHT,
      width: WIDTH,
      alignItems: 'center',
      justifyContent: 'center',
    },

    LogoContainer:{
      alignItems:'center',
    },
    form:{
      width:WIDTH,
      paddingHorizontal: 45,
      marginTop: 36,

    },
    description:{
      marginTop: 37.5,
      paddingHorizontal: 63,
      alignItems: 'center',
      textAlign: 'center',

      
    }
});