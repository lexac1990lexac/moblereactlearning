import { StyleSheet, NativeModules, Platform, Dimensions } from 'react-native';


const {height: HEIGHT} = Dimensions.get('window');

export default StyleSheet.create({
    loadAvatar: {
        marginTop: 20,
    },

    form: {
        marginTop: 50,
    },
    
    wrap: {
        justifyContent: "center",
        height: HEIGHT
    },

    backButton: {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 15
    },

    backButtonText: {
        marginBottom: 2
    }
});