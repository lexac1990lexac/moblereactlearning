import React from 'react';
import { Text, SafeAreaView, View, Image, TextInput, Pressable } from 'react-native';
import Template from '../styles/template'; 
import Shapes from '../styles/shapes'; 
import Avatar from '../assets/AvatarProt.png';
import PageStyles from '../styles/registerPage';
import Constants from '../constants/index';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Button from '../components/Button';
import { Ionicons, AntDesign } from '@expo/vector-icons';

export default function Register({ navigation, navigation: { goBack } }) {
    const avatarBorderStyle = {
        bgColor: "#1d1d1d",
        width: 150,
        height: 150,
    }

    return (
        <SafeAreaView style={Template.AndroidSafeArea}>
            <KeyboardAwareScrollView>
                <View style={PageStyles.wrap}>

                    <Pressable style={PageStyles.backButton} onPress={() => goBack()}>
                        <AntDesign name="left" size={18} color="white" />
                        <Text style={{...Template.defaultText, ...PageStyles.backButtonText}}> Назад</Text>
                    </Pressable>

                    <View style={Template.centered}>
                        <View style={Shapes(avatarBorderStyle).circle}>
                            <Image source={Avatar} style={Template.centered}></Image>
                        </View>
                        <Text style={{...Template.defaultText, ...PageStyles.loadAvatar}}>Загрузить аватар</Text>
                    </View>

                    <View style={PageStyles.form}>
                        <View>
                            <Text style={{...Template.defaultText, ...Template.inputLabel}}>Имя</Text>
                            <TextInput style={Template.defaultInput} placeholder={"Введите имя"} placeholderTextColor={Constants.colors.PLACEHOLDER_COLOR}></TextInput>
                        </View>
                        <View style={Template.formGroup}>
                            <Text style={{...Template.defaultText, ...Template.inputLabel}}>Фамилия</Text>
                            <TextInput style={Template.defaultInput} placeholder={"Введите фамилию"} placeholderTextColor={Constants.colors.PLACEHOLDER_COLOR}></TextInput>
                        </View>
                        <View style={Template.formGroup}>
                            <Text style={{...Template.defaultText, ...Template.inputLabel}}>Почта</Text>
                            <TextInput style={Template.defaultInput} placeholder={"Введите почту"} placeholderTextColor={Constants.colors.PLACEHOLDER_COLOR}></TextInput>
                        </View>
                        <View style={Template.formGroup}>
                            <Text style={{...Template.defaultText, ...Template.inputLabel}}>Пароль</Text>
                            <TextInput style={Template.defaultInput} placeholder={"Введите пароль"} placeholderTextColor={Constants.colors.PLACEHOLDER_COLOR} secureTextEntry={true}></TextInput>
                        </View>
                        <View style={Template.formGroup}>
                            <Text style={{...Template.defaultText, ...Template.inputLabel}}>Подтверждение пароля</Text>
                            <TextInput style={Template.defaultInput} placeholder={"Подтвердите пароль"} placeholderTextColor={Constants.colors.PLACEHOLDER_COLOR} secureTextEntry={true}></TextInput>
                        </View>

                        <Button text={"Зарегистрироваться"}></Button>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        </SafeAreaView>
    );
}