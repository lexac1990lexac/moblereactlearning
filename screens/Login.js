import React from 'react';
import { Text, View, ImageBackground, Image, TextInput, SafeAreaView, ARTText } from 'react-native';
import bgImage from '../assets/Background1.png';
import logo from '../assets/Logo.png';
import PageStyles from '../styles/loginPage'
import Template from '../styles/template';
import Constants from '../constants/index';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Button from '../components/Button';
import TextDelimiter from '../components/TextDelimiter';
import FacebookLogo from '../assets/facebook.png';
import GoogleLogo from '../assets/google.png';

export default function Login({ navigation }){

    return(
      <SafeAreaView style={Template.AndroidSafeArea}>
        <KeyboardAwareScrollView>
          <ImageBackground source={bgImage} style={PageStyles.backgroundContainer}>

            <View style={{...Template.backgroundImageOpacity}}>
            </View>
              <View style={PageStyles.logoContainer}>
                <Image source={logo}/>
              </View>

              <View style={PageStyles.form}>
                <View>
                  <Text style={{...Template.defaultText, ...Template.inputLabel}}>Почта</Text>
                  <TextInput style={Template.defaultInput} placeholder={"Введите почту"} placeholderTextColor={Constants.colors.PLACEHOLDER_COLOR}></TextInput>
                </View>
                <View style={Template.formGroup}>
                  <Text style={{...Template.defaultText, ...Template.inputLabel}}>Пароль</Text>
                  <TextInput style={Template.defaultInput} placeholder={"Введите пароль"} placeholderTextColor={Constants.colors.PLACEHOLDER_COLOR} secureTextEntry={true}></TextInput>
                </View>

                <Button text={"Войти"}></Button>

                <TextDelimiter text={"Через соцсети"}></TextDelimiter>
              </View>

              <View style={PageStyles.socialLinks}>
                <Image style={PageStyles.socialLinkImage} source={FacebookLogo}/>
                <Image style={PageStyles.socialLinkImage} source={GoogleLogo}/>
              </View>

              <View style={PageStyles.createAccountBlock}>
                <Text style={{...Template.defaultText, ...PageStyles.registerButtonHeader }}>Ещё нету аккаунта?</Text>
                <Text style={{...Template.defaultText, ...PageStyles.registerButton, ...Template.upperCase }} onPress={() => navigation.navigate('Register')}>Регистрация</Text>
              </View>
            
          </ImageBackground>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
}