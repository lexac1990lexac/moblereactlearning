import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, TextInput, Dimensions, SafeAreaView, Pressable } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
import bgImage from '../assets/Background1.png';
import logo from '../assets/Logo.png';
import PageStyles from '../styles/forgotPage'
import Template from '../styles/template';
import string from '../constants/string';
import constants from '../constants';
import Button from '../components/Button';
import { Ionicons, AntDesign } from '@expo/vector-icons';


const {width: WIDTH} = Dimensions.get('window')


export default function ForgotPassword({ navigation, navigation: { goBack } }){

    return(
      <SafeAreaView  style = {Template.AndroidSafeArea}>
        <KeyboardAwareScrollView>
          <View style={Template.wrap}>
            
                    {/* <Pressable style={Template.backButton} onPress={() => goBack()}>
                        <AntDesign name="left" size={18} color="white" />
                        <Text style={{...Template.defaultText, ...Template.backButtonText}}> Назад</Text>
                    </Pressable> */}

              <View style = {PageStyles.LogoContainer}>
                <Image source= {logo}/> 
              </View>

              <View style={PageStyles.description}>
                <Text style={{...Template.defaultHeader}}>{string.forgotPasswordPage.header}</Text>
                <Text style={{...Template.smallHeader}}>{string.forgotPasswordPage.subHeader}</Text>
              </View>

              <View style={PageStyles.form}>  
                <View>
                    <Text style={{...Template.defaultText, ...Template.inputLabel}}>{string.form.field.email}</Text>
                  <TextInput  
                    style={Template.defaultInput}
                      placeholder={string.form.field.email}
                      placeholderTextColor={constants.colors.PLACEHOLDER_COLOR}
                  />
                    <Button text={"Отправить письмо"}></Button>
                </View>
              </View>
            </View>
        </KeyboardAwareScrollView> 
      </SafeAreaView>
    );
}