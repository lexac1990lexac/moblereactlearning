import Api from './api';
import StringC from './string';
import Colors from './colors';

export default {
    api: Api,
    string: StringC,
    colors: Colors,
}