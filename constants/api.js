export default {
    API_ROOT: 'https://reactnative.dev',
    IMAGE_UPLOAD_URL: null,
    NEWS_URL: '/movies.json'
}