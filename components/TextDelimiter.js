import { Text, View } from 'react-native';
import React from 'react';
import Template from '../styles/template';

export default function TextDelimiter(props) {
    return (
        <View style={Template.delimiterWrap}>
            <View style={Template.delimiterLine} />
            <Text style={Template.delimiterText}>{props.text ?? "Разделитель"}</Text>
            <View style={Template.delimiterLine} />
        </View>
    );
};