import { Pressable, Text } from "react-native";
import React from 'react';
import Template from '../styles/template';

export default function Button(props) {
    return (
        <Pressable style={props.style ?? Template.defaultButton} onPress={() => props.onPress ?? {}}>
            <Text style={props.textStyle ?? Template.defaultButtonText}>{props.text ?? "Кнопка"}</Text>
        </Pressable>
    );
};